<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});

Route::get('/about', function () {
	$nama = 'Elora Dosi';
    return view('about',['nama'=>$nama]);
});

Route::get('/mahasiswa', function () {
 $mahasiswa=[
 'andi','ida','lala','lili'
 ];
 return view('mahasiswa',['data'=>$mahasiswa]);
});

Route::get('/mahasiswa_c',[\App\Http\Controllers\MahasiswaController::class,'index']);